package prog.ud3.classwork;

/**
 *
 * @author sergio
 */
public class Activitat14 {

    public static void main(String[] args) {
        int contador = 10;
        while (contador >= 0) {
            System.out.println(contador);
            contador--;
        }
        System.out.println("Preparant la partida");
    }
}
