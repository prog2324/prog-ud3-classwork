package prog.ud3.classwork;

/**
 *
 * @author sergio
 */
public class Activitat16 {

    public static void main(String[] args) {
        int contador = 1;
        int suma = 0;

        while (contador <= 1000) {
            suma += contador;
            contador++;
        }
        System.out.println("Resultado: " + suma);
    }
}
