package prog.ud3.classwork;

import java.util.Scanner;

public class Activitat8 {

    final static float CUOTA_GENERAL = 500f;
    final static float DESCUENTO_MENORES_18 = 0.25f;
    final static float DESCUENTO_MAYORES_65 = 0.45f;

    public static void main(String[] args) {
        Scanner teclat = new Scanner(System.in);

        System.out.print("Introduce nombre:");        
            String nombre = teclat.next();
        
        System.out.print("Introduce edad:");
        byte edad;
        // Afegit per a activitat9
        if (teclat.hasNextByte()) {
             edad = teclat.nextByte();
        }
        else {
            System.out.println("Debes introducir un número entero");
            return;
        }
        
        float totalAPagar;
        if (edad < 0 || edad > 120) {
            System.out.println("Edad incorrecta");
        } else {
            if (edad < 18) {
                totalAPagar = CUOTA_GENERAL * (1 - DESCUENTO_MENORES_18);
            } else if (edad > 65) {
                totalAPagar = CUOTA_GENERAL * (1 - DESCUENTO_MAYORES_65);
            } else {
                totalAPagar = CUOTA_GENERAL;
            }
            System.out.printf("%s, tienes que pagar %.1f euros.\n", nombre, totalAPagar);
        }

        teclat.close();

    }
}
