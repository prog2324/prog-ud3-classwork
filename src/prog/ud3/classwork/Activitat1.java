package prog.ud3.classwork;

import java.util.Scanner;

public class Activitat1 {
    final static byte MAYORIA_EDAD = 18;
    
    public static void main(String[] args) {
        System.out.print("Dime tu edad: ");
        Scanner teclat = new Scanner(System.in);
        byte edad = teclat.nextByte();
        
        if (edad < MAYORIA_EDAD) {
            System.out.println("Eres menor de edad");
        } else {
            System.out.println("Eres mayor de edad");
        }
        
        teclat.close();
    }
}
