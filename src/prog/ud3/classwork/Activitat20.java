package prog.ud3.classwork;

import java.util.Scanner;

/**
 *
 * @author sergio
 */
public class Activitat20 {

    public static void main(String[] args) {
        int tablaMult;

        Scanner teclat = new Scanner(System.in);
        System.out.print("Escriu un enter (1-10): ");
        tablaMult = teclat.nextInt();
        if (tablaMult >= 1 && tablaMult <= 10) {
            System.out.println("Taula del " + tablaMult);
            for (int contador = 1; contador <= 10; contador++) {
                System.out.printf("%d * %d = %d\n", tablaMult, contador, tablaMult * contador);
            }
            
//            int contador = 1;
//            do {
//                System.out.printf("%d * %d = %d\n", tablaMult, contador, tablaMult * contador);
//                contador++;
//            } while (contador <= 10);
        }
    }
}
