package prog.ud3.classwork;

import java.util.Scanner;

/**
 *
 * @author sergio
 */
public class Activitat5 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Dime tu nota:");
        byte nota = scanner.nextByte();
        String resultado;

        switch (nota) {
            case 0, 1, 2 -> resultado = "MOLT DEFICIENT";
            case 3, 4 -> resultado = "INSUFICIENT";
            case 5 -> resultado = "SUFICIENT";
            case 6 -> resultado = "BE";
            case 7, 8 -> resultado = "NOTABLE";
            case 9, 10 -> resultado = "EXCEL·LENT";
            default -> resultado = "Nota no válida";
        };

        System.out.println(resultado);

    }
}
