package prog.ud3.classwork;

import java.util.Scanner;

/**
 *
 * @author sergio
 */
public class Activitat18 {

    public static void main(String[] args) {
        int tablaMult, contador = 1;

        Scanner teclat = new Scanner(System.in);
        System.out.print("Escriu un enter (1-10): ");
        tablaMult = teclat.nextInt();
        if (tablaMult >= 1 && tablaMult <= 10) {
            System.out.println("Taula del " + tablaMult);

            do {
                System.out.printf("%d * %d = %d\n", tablaMult, contador, tablaMult * contador);
                contador++;
            } while (contador <= 10);
        }
    }
}
