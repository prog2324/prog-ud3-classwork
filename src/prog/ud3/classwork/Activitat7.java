package prog.ud3.classwork;

import java.util.Scanner;

public class Activitat7 {

    public static void main(String[] args) {

        int num1, num2;
        String missatge;
        
        Scanner teclat = new Scanner(System.in);
        System.out.print("Introduix un número enter: ");
        // Afegit per a activitat9
        if (teclat.hasNextInt()) {
             num1 = teclat.nextInt();
        }
        else {
            System.out.println("Cal introduir un enter");
            return;
        }
                
        System.out.print("Introduix un altre número enter: ");
        // Afegit per a activitat9
        if (teclat.hasNextInt()) {
             num2 = teclat.nextInt();
        }
        else {
            System.out.println("Cal introduir un enter");
            return;
        }
        
        missatge = (num1 > num2)? "num1 es major que num2": "num2 es major o igual a num1";
        System.out.println(missatge);
        
        teclat.close();
        
    }

}
