package prog.ud3.classwork;

import java.util.Scanner;

/**
 *
 * @author sergio
 */
public class Activitat4 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Dime tu nota:");
        byte nota = scanner.nextByte();
        String resultado;

        switch (nota) {
            case 0:
            case 1:
            case 2:
                resultado = "MOLT DEFICIENT";
                break;
            case 3:
            case 4:
                resultado = "INSUFICIENT";
                break;
            case 5:
                resultado = "SUFICIENT";
                break;
            case 6:
                resultado = "BE";
                break;
            case 7:
            case 8:
                resultado = "NOTABLE";
                break;
            case 9:
            case 10:
                resultado = "EXCEL·LENT";
                break;
            default:
                resultado = "Nota no válida";
        }

        System.out.println(resultado);

    }
}
