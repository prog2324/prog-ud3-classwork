package prog.ud3.classwork;

/**
 *
 * @author sergio
 */
public class Activitat15 {

    public static void main(String[] args) {
        int contador = 48;
        while (contador <= 100) {
            System.out.println(contador);
            contador += 2;
        }
    }
}
