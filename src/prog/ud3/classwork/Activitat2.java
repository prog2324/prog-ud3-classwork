package prog.ud3.classwork;

import java.util.Scanner;

public class Activitat2 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Dime un número:");
        int numero = scanner.nextInt();
        String resultado = "";

        if (numero == 0) {
            resultado = "0";
        } else {
            if (numero % 2 == 0) {
                resultado = "par";
            } else {
                resultado = "impar";
            }

            if (numero < 0) {
                resultado += " y negativo";
            } else {
                resultado += " y positivo";
            }
        }
        System.out.println("El número es " + resultado);
    }
}
