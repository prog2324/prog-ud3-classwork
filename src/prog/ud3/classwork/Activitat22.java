package prog.ud3.classwork;

/**
 *
 * @author sergio
 */
public class Activitat22 {
    public static void main(String[] args) {
        for (int i = 1; i <= 10; i++) {         // recorre todos los números de cada tabla
            System.out.printf("--------- TABLA DEL %d ---------\n", i);
            for (int j = 1; j <= 10; j++) {     // recorre los números a multiplicar
                System.out.printf("%d * %d = %d\n", i, j, i*j);                
            } 
            System.out.println();
        }
    }
}
