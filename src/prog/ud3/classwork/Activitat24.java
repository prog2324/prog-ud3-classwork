package prog.ud3.classwork;

/**
 *
 * @author sergio
 */
public class Activitat24 {

    final static int NUM_TIRADES = 1000000;

    public static void main(String[] args) {
        double num;
        int numAleatori, numCares = 0, numCreus = 0;

        for (int i = 0; i < NUM_TIRADES; i++) {
            num = Math.random();
            numAleatori = (int) (num * 2 + 1);
            if (numAleatori == 1) { // cara
                numCares++;
            } else {                  // creu
                numCreus++;
            }
        }
        System.out.println("Número de tirades: " + NUM_TIRADES);
        System.out.println("--------------------------");
        System.out.println("Número de cares: " + numCares);
        System.out.println("Número de creus: " + numCreus);
    }
}
