package prog.ud3.classwork;

import java.util.Scanner;

/**
 *
 * @author sergio
 */
public class Activitat3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Dime tu nota:");
        byte nota = scanner.nextByte();
        String resultado;
        
        if (nota > 10 || nota < 0) {  
            // camino: nota no válida
            System.out.println("Nota no válida");
        } 
        else {   
            // camino: nota valida           
            if (nota < 3) {
                resultado = "MOLT DEFICIENT";
            }
            else if (nota < 5) {
                resultado = "INSUFICIENT";
            }
            else if (nota < 6) {
                resultado = "SUFICIENT";
            }
            else if (nota < 7) {
                resultado = "BE";
            }
            else if (nota < 9) {
                resultado = "NOTABLE";
            }
            else {
                resultado = "EXEL·LENT";
            }
            System.out.println(resultado);
        }
        
        
    }
}
