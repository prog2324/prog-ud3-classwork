package prog.ud3.classwork;

import java.util.Scanner;

public class Activitat10 {

    final static float PREU_BOTELLA_AIGUA = 0.75f;
    final static float PREU_BOTELLA_ENSUCRADA = 1.5f;
    final static float PREU_BOTELLA_ALCOHOL = 3f;
    final static int MESOS_ANY = 12;
    final static float IMPORT_DEVOLUCIO_X_BOTELLA = 0.15f;
    final static float IMPORT_DEVOLUCIO_EXTRA = 0.16f;
    final static float IMPORT_DEVOLUCIO_PLUS = 0.17f;
    final static float PERCENTATGE_VAL_DESCONTE = 0.15f;
    final static float VAL_DESCONTE = 10f;

    public static void main(String[] args) {

        Scanner teclat = new Scanner(System.in);
        int numBotellesAigua, numBotellesEnsucrada, numBotellesAlcohol;

        // SECCIÓN INTRODUCCIÓ DE DADES
        System.out.print("Quantes botelles d'aigua has comprat enguany?: ");
        if (teclat.hasNextInt()) {
            numBotellesAigua = teclat.nextInt();
            if (numBotellesAigua < 0) {
                System.out.println("Error! Número de botelles incorrecte");
                return;
            }
        } else {
            System.out.println("Error! El tipus de dades introduït és incorrecte");
            return;
        }

        System.out.print("Quantes botelles ensucrades has comprat enguany?: ");
        if (teclat.hasNextInt()) {
            numBotellesEnsucrada = teclat.nextInt();
            if (numBotellesEnsucrada < 0) {
                System.out.println("Error! Número de botelles incorrecte");
                return;
            }
        } else {
            System.out.println("Error! El tipus de dades introduït és incorrecte");
            return;
        }

        System.out.print("Quantes botelles alcohòliques has comprat enguany?: ");
        if (teclat.hasNextInt()) {
            numBotellesAlcohol = teclat.nextInt();
            if (numBotellesAlcohol < 0) {
                System.out.println("Error! Número de botelles incorrecte");
                return;
            }
        } else {
            System.out.println("Error! El tipus de dades introduït és incorrecte");
            return;
        }

        // SECCIÓN CALCULS I EIXIDA
        float gastoAnual, mitjanaMensual;
        gastoAnual = numBotellesAigua * PREU_BOTELLA_AIGUA;
        gastoAnual += numBotellesEnsucrada * PREU_BOTELLA_ENSUCRADA;
        gastoAnual += numBotellesAlcohol * PREU_BOTELLA_ALCOHOL;
        mitjanaMensual = gastoAnual / MESOS_ANY;
        System.out.printf("Has gastat una mitjana de %.2f€ cada mes\n", mitjanaMensual);
        
        float dinersDevolucioBotelles;
        dinersDevolucioBotelles = (numBotellesAigua + numBotellesEnsucrada + numBotellesAlcohol) * IMPORT_DEVOLUCIO_X_BOTELLA;
        System.out.printf("Al reciclar les botelles has obtingut %.2f€ i ",dinersDevolucioBotelles);
        
        if (gastoAnual >= 250) {
            System.out.printf("un val/descompte de %.2f€", (gastoAnual * PERCENTATGE_VAL_DESCONTE));
        }
        else if (gastoAnual > 100 && gastoAnual < 150) {
            System.out.printf("una bonificació per l'any vinent de %.2f€ per botella", IMPORT_DEVOLUCIO_EXTRA - IMPORT_DEVOLUCIO_X_BOTELLA);
        }
        else if (gastoAnual >= 150 && gastoAnual < 250) {
            System.out.printf("una bonificació per l'any vinent de %.2f€ per botella", IMPORT_DEVOLUCIO_PLUS - IMPORT_DEVOLUCIO_X_BOTELLA);
        }
        else if (gastoAnual >= 10 && gastoAnual <= 20) {
            System.out.printf("un val/descompte de %.2f€", VAL_DESCONTE);
        }
        else {
            System.out.println("no reps cap bonificació");
        }
        System.out.println();
        teclat.close();

    }
}
