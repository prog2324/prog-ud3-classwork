/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package prog.ud3.classwork;

import java.util.Scanner;

/**
 *
 * @author sergio
 */
public class Activitat6 {
    
    public static void main(String[] args) {
        System.out.println("¿Qué tipo de desecho necesitas tirar?");
        System.out.println(" 1. Plástico");
        System.out.println(" 2. Orgánico");
        System.out.println(" 3. Papel");
        System.out.println(" 4. Cartón");
        System.out.println(" 5. Otros");
        System.out.print("Introduce una opción [1-5]: ");
        Scanner sc = new Scanner(System.in);
        byte opcion = sc.nextByte();
        String contenedor;

        if (opcion >= 1 && opcion <= 5) {
            if (opcion == 1) {
                contenedor = "groc";
            } else if (opcion == 2) {
                contenedor = "gris";
            } else if (opcion == 3 || opcion == 4) {
                contenedor = "blau";
            } else {
                contenedor = "verd";
            }
            
            
//            switch (opcion) {
//                case 1:
//                    contenedor = "groc";
//                    break;
//                case 2:
//                    contenedor = "gris";
//                    break;
//                case 3:
//                case 4:
//                    contenedor = "blau";
//                    break;
//                default:
//                    contenedor = "verd";
//            }
            
//            contenedor = switch (opcion) {
//                case 1 -> "groc";
//                case 2 -> "gris";
//                case 3, 4 -> "blau";
//                default -> "verd";
//            };
            
            System.out.println("Debes tirarlo al contenedor " + contenedor);
            
        } else {
            System.out.println("Opción no válida. Vuelve a intentarlo");
        }

    }

}
