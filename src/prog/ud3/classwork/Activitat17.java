package prog.ud3.classwork;

import java.util.Scanner;

/**
 *
 * @author sergio
 */
public class Activitat17 {
    final static int NUM_SECRETO = 10;
    public static void main(String[] args) {
        Scanner teclat = new Scanner(System.in);
        int numero;

        do {
            System.out.print("Dime número: ");
            numero = teclat.nextInt();
        } while (numero != NUM_SECRETO);
    }

}
