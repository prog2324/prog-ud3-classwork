package prog.ud3.classwork;

/**
 *
 * @author sergio
 */
public class Activitat21 {

    public static void main(String[] args) {
        int parells = 0, imparells = 0, multDe5 = 0;

        for (int i = 1; i <= 100; i++) {
            if (i % 2 == 0) {  
                parells++;
            } else {
                imparells++;
            }
            if (i % 5 == 0) {
//                System.out.println(i);
                multDe5++;
            }
        }
        System.out.println("Parells: " + parells);
        System.out.println("Imparells: " + imparells);
        System.out.println("Multiplos de 5: " + multDe5);
    }
}
