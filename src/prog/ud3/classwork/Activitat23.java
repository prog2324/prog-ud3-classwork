
package prog.ud3.classwork;

import java.util.Scanner;

/**
 *
 * @author sergio
 */
public class Activitat23 {
    public static void main(String[] args) {
        Scanner teclat = new Scanner(System.in);
        int num;
        for (int i = 1; i <= 50; i++) {
            System.out.printf("Introdueix número (%d): ",i);
            num = teclat.nextInt();
            if (num == 100) {
                continue;
            }
            else if (num == 0) {
                break;
            }
            if (num % 2 == 0) {  
                System.out.println("És parell");
            } else {
                System.out.println("És imparell");
            }
            if (num % 5 == 0) {
                System.out.println("És multiple de 5");
            }  
            
        }
    }
}
